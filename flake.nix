{
  description = "atsr python libraries";

  inputs = {
    settings = {
      url = "path:./nix/settings.nix";
      flake = false;
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-utils, ... }@inputs:

    flake-utils.lib.eachDefaultSystem (system: let

      settings = import inputs.settings;
      nixpkgs = inputs.nixpkgs.legacyPackages.${system};
      pythonPackages = nixpkgs.${settings.python+"Packages"};

      thispkg = pythonPackages.buildPythonPackage rec {
        pname = "ply";
        version = "3.4";
        src = pythonPackages.fetchPypi {
          inherit pname version;
          sha256 = "af435f11b7bdd69da5ffbc3fecb8d70a7073ec952e101764c88720cdefb2546b";
        };
      };

      pythonPackaged = nixpkgs.${settings.python}.withPackages (p: with p; [ thispkg ]);

    in {

      defaultPackage = thispkg;
        
      devShell = nixpkgs.mkShell {
        buildInputs = [ pythonPackaged ];
        shellHook = ''
          export PYTHONPATH=${pythonPackaged}/${pythonPackaged.sitePackages}
        '';
      };

    });
}
